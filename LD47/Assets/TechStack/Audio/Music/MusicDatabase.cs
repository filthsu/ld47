﻿using TechStack.DataUtils;
using TechStack.DB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace TechStack.Audio.Music 
{
    [System.Serializable]
    public class Playlist : DataEntry
    {
        public List<AudioClip> m_songs;
    }
    [CreateAssetMenu(fileName = "Music.asset", menuName = "TechStack/Data/Music Database")]
    public class MusicDatabase : DataBase<Playlist>
    {
    }
}