﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace TechStack.Audio.Music
{
    /// <summary>
    /// Starts a music playlist at Start
    /// </summary>
    public class PlayMusicOnLoad : MonoBehaviour
    {
        [Data(typeof(MusicDatabase))]
        [SerializeField] private int m_playlist = 0;
        // Start is called before the first frame update
        void Start()
        {
            if(m_playlist > 0)
            {
                MusicPlayer.Play(m_playlist);
            }
        }

    }
}