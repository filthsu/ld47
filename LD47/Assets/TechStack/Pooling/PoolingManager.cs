﻿using TechStack.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// A global interface to simplify the use of pooling system
/// </summary>
public class PoolingManager
{
    static PoolContainer s_poolContainer;

    public static void Initialize(PoolDatabase data)
    {
        if (null != s_poolContainer) return;
        s_poolContainer = new GameObject("Pools").AddComponent<PoolContainer>();
        GameObject.DontDestroyOnLoad(s_poolContainer);
        s_poolContainer.data = data;
    }
    static GameObject t_obj;
    public static GameObject Pop(int ID, Vector3 position, Quaternion rotation, bool activate = true)
    {
        t_obj = s_poolContainer.Pop(ID);
        t_obj.transform.position = position;
        t_obj.transform.rotation = rotation;
        if(activate)
            t_obj.SetActive(true);
        return t_obj;
    }

    public static GameObject Pop(int ID, Transform transform, bool activate = true)
    {
        return Pop(ID, transform.position, transform.rotation, activate);
    }

    public static GameObject Pop(int ID, Vector3 position, bool activate = true)
    {
        return Pop(ID, position, Quaternion.identity, activate);
    }
}