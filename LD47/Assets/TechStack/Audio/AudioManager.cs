﻿using UnityEngine;
using System.Collections.Generic;
using TechStack.Pooling;
using TechStack.DataUtils;

namespace TechStack.Audio
{
    public static class AudioManager
    {
        private static float s_volume = -1;
        public static float Volume 
        {
            get
            {
                if (s_volume < 0) s_volume = PlayerPrefs.GetFloat("SFX_VOLUME", 1.0f);
                return s_volume;
            }
            set
            {
                s_volume = value;
                PlayerPrefs.SetFloat("SFX_VOLUME", value);
            }
        }

        public static void Initialize(AudioDatabase data)
        {
            Transform container = new GameObject("Audio Objects").transform;
            GameObject.DontDestroyOnLoad(container);
            AudioObject pref = new GameObject("Audio Object").AddComponent<AudioObject>();
            pref.transform.parent = container;
            m_audioObjectPool = new Pool(container, 32, pref.gameObject);

            m_lookupTable = DataFileUtility.GenerateLookupTable<AudioDataEntry>(data);
        }

        private static Pool m_audioObjectPool;
        private static Audio.AudioDataEntry[] m_lookupTable;

        /// <summary>
        /// Play an audio clip
        /// </summary>
        /// <param name="ID">Audio clip to play</param>
        /// <param name="position">Position in world space</param>
        /// <param name="volume">Clip volume</param>
        /// <param name="spatialBlend">Spatial Blend. 0 = 2D, 1 = 3D</param>
        public static void Play(int ID, Vector3 position, float volume = 1, float spatialBlend = 1)
        {
            if (ID <= 0) return;
            if (null == m_audioObjectPool) return;

            AudioObject src = m_audioObjectPool.Pop().GetComponent<AudioObject>();
            src.Play(m_lookupTable[ID], position, volume * Volume);
        }
    }
}