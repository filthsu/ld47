﻿using TechStack.VFX;
using TechStack.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A global interface to simplify the use of VFX system
/// </summary>
public class VFXManager
{

    static PoolContainer s_poolContainer;

    public static void Initialize(VFXDatabase data)
    {
        if (null != s_poolContainer) return;
        s_poolContainer = new GameObject("VFX").AddComponent<PoolContainer>();
        s_poolContainer.data = data;
    }

    static GameObject s_obj;
    static VFXComponent s_vfx;

    public static void Play(int ID, Vector3 position, Quaternion rotation, Vector3 offset, Transform bindTransform = null)
    {
        s_obj = s_poolContainer.Pop(ID);
        if (null == s_obj) return;
        s_obj.transform.position = position + offset;
        s_obj.transform.rotation = rotation;
        s_vfx = s_obj.GetComponent<VFXComponent>();
        if(null != bindTransform)
        {
            if(null != bindTransform)
            {
                s_obj.transform.position = bindTransform.position;
                s_obj.transform.rotation = bindTransform.rotation;
            }
            s_vfx.BindTransform = bindTransform;
            s_vfx.BindOffset = offset;
            
        }
        s_obj.SetActive(true);
    }

    public static void Play(int ID, Transform transform, Vector3 offset, Transform bindTransform = null)
    {
        Play(ID, transform.position, transform.rotation, offset, bindTransform);
    }

    public static void Play(int ID, Vector3 position, Vector3 offset, Transform bindTransform = null)
    {
        Play(ID, position, Quaternion.identity, offset, bindTransform);
    }
}
