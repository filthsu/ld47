﻿using System.Collections;

using System.Collections.Generic;
using UnityEngine;

public class LD47UIState : MonoBehaviour
{
    private CanvasGroup m_canvas;
    public CanvasGroup Canvas
    {
        get
        {
            if (null == m_canvas) m_canvas = GetComponent<CanvasGroup>();
            if (null == m_canvas) m_canvas = gameObject.AddComponent<CanvasGroup>();
            return m_canvas;
        }
    }

    private TechStack.UI.UIState m_state;
    // Start is called before the first frame update
    void Start()
    {
        m_state = GetComponent<TechStack.UI.UIState>();
        m_state.OnStateChanged += this.OnStateChanged;
    }

    void OnStateChanged(bool newstate) 
    {
    }

    // Update is called once per frame
    void Update()
    {
        Canvas.alpha = Mathf.MoveTowards(Canvas.alpha, (m_state.IsCurrent ? 1 : 0), Time.unscaledDeltaTime * 10);
    }
}
