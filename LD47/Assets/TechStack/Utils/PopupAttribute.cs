﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PopupAttribute : PropertyAttribute
{
    public string[] m_options;
}
