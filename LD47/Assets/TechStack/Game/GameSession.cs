﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace TechStack.Game
{
    public class GameSession : MonoBehaviour
    {
        protected internal virtual IEnumerator Initialize() { yield return null; }
        protected internal virtual IEnumerator Run() { yield return null; }
        protected internal virtual IEnumerator Cleanup() { yield return null; }
    }
}