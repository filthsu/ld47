﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System.IO;
using TechStack.App;

namespace TechStack.Editor
{
    public class Utils
    {
        [MenuItem("TechStack/Utils/Create Initial Scenes")]
        public static void CreateScenes()
        {
            string[] sceneNames = new string[]
            {
                "Launch",
                "Init",
                "Menu"
            };

            EditorBuildSettingsScene[] buildScenes = new EditorBuildSettingsScene[sceneNames.Length];

            for (int i = 0; i < sceneNames.Length; i++)
            {
                Scene scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);

                if (EditorSceneManager.SaveScene(scene, "Assets/Scenes/" + sceneNames[i] + ".unity"))
                {
                    buildScenes[i] = new EditorBuildSettingsScene("Assets/Scenes/" + sceneNames[i] + ".unity", true);
                }
                else
                {
                    Debug.LogError("Couldn't save scene :(");
                }
            }

            EditorBuildSettings.scenes = buildScenes;
            AssetDatabase.SaveAssets();
        }  

        [MenuItem("TechStack/Utils/Create AppData File")]
        public static void CreateAppdata()
        {
            if (AssetDatabase.FindAssets("t:AppData").Length > 0) return;

            AppData appdata = ScriptableObject.CreateInstance<AppData>();
            if (!Directory.Exists(UnityEngine.Application.dataPath + "/Resources"))
            {
                AssetDatabase.CreateFolder("Assets", "Resources");
            }

            AssetDatabase.CreateAsset(appdata, "Assets/Resources/AppData.asset");
            AssetDatabase.SaveAssets();
        }
     }
}