﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace TechStack.Utils
{
    public class DeveloperObject : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

#if UNITY_EDITOR || DEVELOPMENT_BUILD
#else
            gameObject.SetActive(false);
#endif
        }


    }
}