﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorUtil
{
    public static Vector2 xy(this Vector3 vec) { return new Vector2(vec.x, vec.y); }
    public static Vector2 xz(this Vector3 vec) { return new Vector2(vec.x, vec.z); }
    public static Vector3 xz(this Vector2 vec) { return new Vector3(vec.x, 0, vec.y); }
    public static Vector3 xyz(this Vector2 vec) { return new Vector3(vec.x, vec.y, 0); }
    public static Vector3 Mul(this Vector3 a, Vector3 b)
    {
        a.x *= b.x;
        a.y *= b.y;
        a.z *= b.z;
        return a;
    }
    public static Vector2 Cross(this Vector2 a)
    {
        return new Vector2(-a.y, a.x);
    }
}
