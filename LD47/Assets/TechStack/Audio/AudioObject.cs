﻿using UnityEngine;
using System.Collections;

namespace TechStack.Audio
{
    public class AudioObject : MonoBehaviour
    {
        private AudioSource m_audioSource;
        private float m_timer = 0;

        private void Awake()
        {
            m_audioSource = gameObject.AddComponent<AudioSource>();
            m_audioSource.loop = false;
            m_audioSource.playOnAwake = false;
        }

        public void Play(AudioDataEntry entry, Vector3 position, float volume = 1, float spatialBlend = 1)
        {
            transform.position = position;
            m_audioSource.clip = entry.GetRandomClip();
            m_audioSource.volume = volume;
            m_audioSource.spatialBlend = spatialBlend;
            if (null == m_audioSource.clip) return;
            m_audioSource.outputAudioMixerGroup = entry.m_mixer;
            m_timer = m_audioSource.clip.length + .1f;
            gameObject.SetActive(true);
            m_audioSource.Play();
        }

        private void Update()
        {
            if(m_timer > 0)
            {
                m_timer -= Time.deltaTime;
                if(m_timer <= 0)
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }
}