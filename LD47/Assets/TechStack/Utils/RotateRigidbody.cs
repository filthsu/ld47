﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RotateRigidbody : MonoBehaviour
{
    [SerializeField] private Vector3 m_rotationSpeed = Vector3.zero;
    private Rigidbody m_rigidbody;
    // Start is called before the first frame update
    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        m_rigidbody.MoveRotation(m_rigidbody.rotation * Quaternion.Euler(m_rotationSpeed * Time.deltaTime));
    }
}
