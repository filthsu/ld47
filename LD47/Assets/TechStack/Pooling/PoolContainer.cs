﻿using TechStack.DB;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using UnityEngine;
namespace TechStack.Pooling
{
    public class Pool
    {
        internal Transform root;
        private Queue<Poolable> pool;
        private List<Poolable> objects;
        private List<GameObject> prefabs;

        internal GameObject GetPrefab()
        {
            return prefabs[Random.Range(0, prefabs.Count)];
        }

        internal void Cleanup()
        {
            foreach (Poolable p in objects)
            {
                if (p.gameObject.activeSelf)
                {
                    p.gameObject.SetActive(false);
                }
            }

            foreach (Poolable p in objects)
            {
                p.transform.parent = root;
            }
        }

        /*
        public Pool(Transform parent, Pooling.PoolInfo info)
        {
            root = new GameObject(info.name).transform;
            root.parent = parent;
            pool = new Queue<Poolable>();
            objects = new List<Poolable>();
            prefabs = info.prefabs;
            Extend(info.initialSize);
        }
        */
        public Pool(Transform parent, int initialSize, params GameObject[] prefabs)
        {
            pool = new Queue<Poolable>();
            objects = new List<Poolable>();
            this.prefabs = new List<GameObject>(prefabs);
            root = parent;
            Extend(initialSize);
        }

        internal Poolable Pop()
        {
            if (pool.Count <= 0) { Extend(2); }
            return pool.Dequeue();
        }

        internal void Push(Poolable obj)
        {
            pool.Enqueue(obj);
        }

        private void Extend(int count = 1)
        {
            for (int i = 0; i < count; i++)
            {
                int index = Random.Range(0, prefabs.Count);
                Poolable obj = GameObject.Instantiate(prefabs[index], root).AddComponent<Poolable>();
                objects.Add(obj);
                obj.owner = this;
                obj.gameObject.SetActive(false);
            }
        }
    }


    public class PoolContainer : MonoBehaviour
    {
        
        private DataBase<PoolableData> m_data;

        public DataBase<PoolableData> data
        {
            get
            {
                return m_data;
            }
            set
            {
                m_data = value;
                Initialize();
            }
        }

        public void SetData<T>(DataBase<T>data) where T:PoolableData
        {
            this.data = data as DataBase<PoolableData>;
        }
        
        private void Initialize()
        {
            
            if(null != pools)
            {
                for(int i = 0; i < pools.Count; i++)
                {
                    Destroy(pools[i].root.gameObject);
                }
            }

            int size = 1;
            for (int i = 0; i < data.data.Count; i++)
            {
                size = Mathf.Max(size, data.data[i].ID);
            }
            lookupTable = new Pool[size + 1];

            pools = new List<Pool>();

            parent = new GameObject("Objects").transform;
            parent.parent = transform;

            for (int i = 0; i < data.data.Count; i++)
            {
                GameObject container = new GameObject(data.data[i].name);
                container.transform.parent = parent;
                Pool pool = new Pool(container.transform, data.data[i].m_initialSize, data.data[i].m_prefabs);//data.data[i]);
                lookupTable[data.data[i].ID] = pool;
                pools.Add(pool);
            }
            
        }

        Pool[] lookupTable;
        List<Pool> pools;
        Transform parent;

        public GameObject Pop(int ID)
        {
            try
            {
                return lookupTable[ID].Pop().gameObject;
            }
            catch
            {
                return null;
            }
        }
 
    }
}