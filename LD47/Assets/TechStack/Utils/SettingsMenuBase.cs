﻿using System.Collections;
using System.Collections.Generic;
using TechStack.Audio;
using TechStack.Audio.Music;
using TechStack.UI;
using UnityEngine;
using UnityEngine.UI;

namespace TechStack.Utils
{
    public class SettingsMenuBase : MonoBehaviour
    {
        [SerializeField] private Slider m_audioSlider = null;
        [SerializeField] private Slider m_musicSlider = null;
        [SerializeField] private Dropdown m_resolutionSelector = null;
        [SerializeField] private Toggle m_fullscreenToggle = null;
      
        protected virtual void Refresh()
        {
            if (m_audioSlider) m_audioSlider.SetValueWithoutNotify(AudioManager.Volume);
            if (m_musicSlider) m_musicSlider.SetValueWithoutNotify(MusicPlayer.Volume);
            RefreshResolutionDialog();
        }

        private void RefreshResolutionDialog()
        {
            m_resolutionSelector.ClearOptions();
            List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
            for(int i = 0; i < Screen.resolutions.Length; i++)
            {
                options.Add(new Dropdown.OptionData(Screen.resolutions[i].width + " x " + Screen.resolutions[i].height));
            }
            m_resolutionSelector.AddOptions(options);
            m_resolutionSelector.SetValueWithoutNotify(GetCurrentResolutionIndex());
            m_fullscreenToggle.SetIsOnWithoutNotify(Screen.fullScreen);
        }

        private int GetCurrentResolutionIndex()
        {
            int result = 0;

            for(int i = 0; i < Screen.resolutions.Length; i++)
            {
                Resolution res = Screen.resolutions[i];
                if(res.width == Screen.width && res.height == Screen.height)
                {
                    return i;
                }
            }

            return result;
        }

        private void Awake()
        {
            UIState state = GetComponent<UIState>();
            if(null != state)
            {
                state.OnStateChanged += this.OnUIStateChange;
            }
            Refresh();
        }

        private void OnUIStateChange(bool newState)
        {
            if (newState)
            {
                Refresh();
            }
        }

        public virtual void OnAudioVolumeChanged(float val)
        {
            AudioManager.Volume = Mathf.Clamp01(val);
        }

        public virtual void OnMusicVolumeChanged(float val)
        {
            MusicPlayer.Volume = val;
        }


        public void OnApply()
        {
            Resolution res = Screen.resolutions[m_resolutionSelector.value];
            Screen.SetResolution(res.width, res.height, m_fullscreenToggle.isOn);
        }

        public void OnConfirm()
        {
            OnApply();
            PlayerPrefs.Save();
            UIStack.instance.Pop();
        }
    }
}