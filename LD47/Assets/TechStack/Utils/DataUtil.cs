﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;
using TechStack.DB;
using System;

namespace TechStack.DataUtils
{
    [System.Serializable]
    public class DataEntry
    {
        public DataEntry() { }

        public string name;
        [SerializeField] [HideInInspector] private  int m_id;
        public int ID
        {
            get { return m_id; }
            set
            {
#if UNITY_EDITOR
                m_id = value;
#endif
            }
        }
    }

    public interface IDatabase
    {
        List<DataEntry> Data { get; }
    }

    [System.Serializable]
    public struct DataInfo
    {
        public string name;
        public int ID;
    }

    public static class DataFileUtility
    {
        public static T[] GenerateLookupTable<T>(DataBase<T> db) where T : DataEntry
        {
            int size = 1;
            for (int i = 0; i < db.data.Count; i++)
            {
                if (db.data[i].ID > size)
                {
                    size++;
                    i = -1;
                }
            }

            T[] result = new T[size + 1];
            for (int i = 0; i < db.data.Count; i++)
            {
                result[db.data[i].ID] = db.data[i];
            }
            return result;
        }

#if UNITY_EDITOR

        public static DataBase<DataEntry> GetDatabase(System.Type type)
        {
            string[] guids = UnityEditor.AssetDatabase.FindAssets("t:" + type.Name.ToLower());
            if(guids.Length == 0)
            {
                return null;
            }
            
            UnityEngine.Object obj = UnityEditor.AssetDatabase.LoadAssetAtPath(
               UnityEditor.AssetDatabase.GUIDToAssetPath(guids[0]),
               type);



            //(obj as DataBase<DataEntry>).Editor_UpdateLookupLists();

            return obj as DataBase<DataEntry>;
        }

        public static DataInfo[] GetDBNames(System.Type type)
        {
            string[] guids = UnityEditor.AssetDatabase.FindAssets("t:" + type.Name.ToLower());

            if(0 == guids.Length)
            {
                return new DataInfo[]
                {
                    new DataInfo()
                    {
                        ID = 0,
                        name = "Database Not Found"
                    }
                };
            }

            UnityEngine.Object obj = UnityEditor.AssetDatabase.LoadAssetAtPath(
                UnityEditor.AssetDatabase.GUIDToAssetPath(guids[0]),
                type);

            PropertyInfo prop = type.GetProperties().Single(Ping => Ping.Name == "Data");
            if (null == prop) Debug.LogError("null prop");

            IEnumerable<DataEntry> val = (prop.GetValue(obj, null)) as IEnumerable<DataEntry>;
            List<DataEntry> dataList = new List<DataEntry>((from v in val select v).Cast<DataEntry>().ToArray());
            return GetNames<DataEntry>(dataList);
        }

        public static DataInfo[] GetNames<T>(List<T> data) where T : DataEntry
        {
            DataInfo[] result = new DataInfo[data.Count + 1];
            result[0] = new DataInfo() { name = "None", ID = 0 };

            for (int i = 0; i < data.Count; i++)
            {
                result[i + 1] = new DataInfo()
                {
                    name = data[i].name,
                    ID = data[i].ID
                };
            }
            return result;
        }

        public static int FindID(int index, DataInfo[] data)
        {
            return data[index].ID;
        }

        public static int FindIndex(int ID, DataInfo[] data)
        {
            for(int i = 0; i <data.Length; i++)
            {
                if (data[i].ID == ID) return i;
            }
            return 0;
        }

        public static void AssignIDs<T>(List<T> data) where T : DataEntry
        {
            for (int i = data.Count - 1; i > -1; i--)
            {
                for (int j = 0; j < data.Count; j++)
                {
                    if (i == j) continue;
                    if (null == data[i] || null == data[j]) continue;
                    if (data[i].ID == data[j].ID)
                    {
                        data[i].ID = -1;
                    }
                }
            }

            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].ID <= 0)
                {
                    data[i].ID = NextID(data);
                }
            }
        }

        private static int NextID<T>(List<T> data) where T : DataEntry
        {
            int result = 1;
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].ID == result)
                {
                    i = -1;
                    result++;
                }
            }
            return result;
        }

        public static int ID2Index<T>(List<T> data, int ID) where T: DataEntry
        {
            for(int i = 0; i < data.Count; i++)
            {
                if(data[i].ID == ID)
                {
                    return i;
                }
            }
            return -1;
        }

        public static int Index2ID<T>(List<T> data, int index) where T : DataEntry
        {
            return data[index].ID;
        }

      
#endif
#if UNITY_EDITOR

        public static T GetDatabase<T>() where T: ScriptableObject
        {
            System.Type type = typeof(T);
            string[] guids = UnityEditor.AssetDatabase.FindAssets("t:" + type.Name);
            if (guids.Length > 0)
            {
                 return UnityEditor.AssetDatabase.LoadAssetAtPath<T>(UnityEditor.AssetDatabase.GUIDToAssetPath(guids[0]));
            }
            return null;
        }

        public static void WriteConsts<T>(List<T>data) where T: DataEntry
        {
            string path = UnityEditor.EditorUtility.SaveFilePanel("Export Consts", Application.dataPath, typeof(T).Name, "cs");
            if (string.IsNullOrEmpty(path)) return;

            string className = Path.GetFileNameWithoutExtension(path);

            StreamWriter writer = new StreamWriter(File.Create(path));
            writer.WriteLine("public static class " + className + "{");
            for(int i = 0; i< data.Count; i++)
            {
                string name = data[i].name.Replace(' ', '_');
                writer.WriteLine("public static int " + name + " { get { return " + data[i].ID + "; } }");
            }
            writer.WriteLine("}");
            writer.Close();
            UnityEditor.AssetDatabase.Refresh();
        }

        public class ConstDataPair
        {
            public string key, value;
        }

        public static void WriteConsts(string path, string className, System.Type valueType, ConstDataPair[] data)
        {
            StreamWriter writer = new StreamWriter(File.Create(path));
            writer.WriteLine("public static class " + className + "{");
            for (int i = 0; i < data.Length; i++)
            {
                string name = data[i].key.Replace(' ', '_');
                writer.WriteLine("public static " + valueType.FullName + " " + name + " { get { return " + @"""" + data[i].value + @"""; } }");
            }
            writer.WriteLine("}");
            writer.Close();
            UnityEditor.AssetDatabase.Refresh();
        }

        public static void WriteConsts(string className, System.Type valueType, ConstDataPair[] data)
        {
            string path = UnityEditor.EditorUtility.SaveFilePanel("Select folder", Application.dataPath, className, "cs");
            if (string.IsNullOrEmpty(path)) return;
            WriteConsts(path, className, valueType, data);
        }
#endif
    }
}