﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusRoute : MonoBehaviour
{
    [SerializeField] private Color m_color = Color.blue;

    public List<GameObject> m_nodes;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [EditorButton("Add Node")]
    public void AddNode()
    {
        if (null == m_nodes) m_nodes = new List<GameObject>();
        GameObject go = new GameObject(gameObject.name + " node");
        go.transform.parent = transform;
        go.transform.position = transform.position;
        m_nodes.Add(go);
    }

    private void OnDrawGizmos()
    {
        for (int i = m_nodes.Count - 1; i > -1; i--)
        {
            if (null == m_nodes[i])
            {
                m_nodes.RemoveAt(i);
            }
        }
        for(int i = 0; i < m_nodes.Count; i++)
        {
            int j = (i + 1) % m_nodes.Count;
            Debug.DrawLine(m_nodes[i].transform.position, m_nodes[j].transform.position,m_color);
        }
    }
}
