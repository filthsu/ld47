﻿using TechStack.DataUtils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace TechStack.Audio
{
    [System.Serializable]
    public class AudioDataEntry : DataEntry
    {
        public AudioMixerGroup m_mixer;
        public List<AudioClip> m_clips;

        public AudioClip GetRandomClip() 
        {
            try
            {
                return m_clips[Random.Range(0, m_clips.Count)];
            }
            catch { return null; }
        }
    }
    [CreateAssetMenu(fileName = "Audio.asset", menuName = "TechStack/Data/Audio Database")]
    public class AudioDatabase : TechStack.DB.DataBase<AudioDataEntry>
    {

    }
}