﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LD47Character : MonoBehaviour
{
    public Bus Bus { get; set; }
    public BusStop BusStop { get; set; }

    [SerializeField] private GameObject m_renderer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_renderer.SetActive(null == Bus);
    }
}
