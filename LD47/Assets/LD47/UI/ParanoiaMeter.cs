﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ParanoiaMeter : MonoBehaviour
{
    [SerializeField] private Image m_progressBar;
    [SerializeField] private Gradient m_progressBarColor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(null != LD47GameSession.Instance)
        {
            m_progressBar.fillAmount = LD47GameSession.Instance.Paranoia;
            m_progressBar.color = m_progressBarColor.Evaluate(LD47GameSession.Instance.Paranoia);
        }
    }
}
