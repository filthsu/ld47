﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusStop : MonoBehaviour
{
    [SerializeField] private bool m_playerStartPoint = false;
    public bool IsStartPoint { get { return m_playerStartPoint; } }
    public List<LD47Character> Characters { get; set; }
    [SerializeField] private float m_radius = 5;
    public void Add(LD47Character character)
    {
        Characters.Add(character);
        character.BusStop = this;
        character.transform.position = transform.position +
            new Vector3(Random.value - .5f, 0, Random.value - .5f).normalized *
            Random.value * m_radius;
    }

    public void Remove(LD47Character character)
    {
        Characters.Remove(character);
        character.BusStop = null;
    }

    public Bus ScanBus()
    {
        int layer = (1 << 9);
        Collider[] c = Physics.OverlapSphere(transform.position, 10, layer);
        Bus result = null;
        float maxDot = 0;
        float minDistance = 11;
        if (c.Length > 0)
        {
            for (int i = 0; i < c.Length; i++)
            {
                Bus sample = c[i].GetComponentInParent<Bus>();
                Vector3 delta = sample.transform.position - transform.position;
                float dot = Vector3.Dot(transform.forward, delta.normalized);
                if (dot > maxDot)
                {
                    if (delta.magnitude < minDistance)
                    {
                        maxDot = dot;
                        minDistance = delta.magnitude;
                        result = sample;
                    }
                }
            }
        }
        return result;
    }

    // Start is called before the first frame update
    void Start()
    {
        Characters = new List<LD47Character>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, m_radius);
        Gizmos.DrawWireSphere(transform.position, 10);
    }
}
