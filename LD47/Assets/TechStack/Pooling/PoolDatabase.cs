﻿using TechStack.DataUtils;
using TechStack.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TechStack.Pooling
{
    [CreateAssetMenu(fileName = "Pool.asset", menuName = "TechStack/Data/Pool Database")]
    public class PoolDatabase : TechStack.DB.DataBase<PoolableData>
    {
    }
}