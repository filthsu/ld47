﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TechStack.App
{
    public class Launcher : MonoBehaviour
    {
        [RuntimeInitializeOnLoadMethod]
        private static void AppEntryPoint()
        {
#if UNITY_EDITOR
            if(SceneManager.GetActiveScene().buildIndex != 0)
            {
                SceneManager.LoadScene(0);
            }
#endif
            Application app = new GameObject("Application Lifecycle").AddComponent<Application>();
        }
    }
}