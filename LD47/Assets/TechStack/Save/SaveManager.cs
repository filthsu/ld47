﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
namespace TechStack.Save
{
    public class SaveManager 
    {
        public static bool Save(object data, string name)
        {
            try
            {
                string json = JsonUtility.ToJson(data);
                if (!Directory.Exists(Application.persistentDataPath + "/Saves"))
                {
                    Directory.CreateDirectory(Application.persistentDataPath + "/Saves");
                }

                using (StreamWriter file = new StreamWriter(Application.persistentDataPath + "/Saves"))
                {
                    file.Write(json);
                }
                return true;
            }
            catch(System.Exception e)
            {
                Debug.LogError(e.Message + " " + e.StackTrace);
            }
            return false;
        }

        public static bool Load<T>(string name, out T result)
        {
            if (!Directory.Exists(Application.persistentDataPath + "/Saves"))
            {
                Directory.CreateDirectory(Application.persistentDataPath + "/Saves");
            }

            try
            {
                string json = File.ReadAllText(Application.persistentDataPath + "/Saves/" + name);
                result = JsonUtility.FromJson<T>(json);
                if(null != result)
                {
                    return true;
                }
            }
            catch(System.Exception e)
            {
                Debug.LogError(e.Message + " " + e.StackTrace);
            }
            
            result = default(T);
            return false;
        }

        public static string[] ListSaves()
        {
            if (!Directory.Exists(Application.persistentDataPath + "/Saves"))
            {
                Directory.CreateDirectory(Application.persistentDataPath + "/Saves");
            }
            return Directory.GetFiles(Application.persistentDataPath + "/Saves");
        }
    }
}