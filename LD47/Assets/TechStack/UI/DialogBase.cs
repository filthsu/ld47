﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TechStack.UI
{
    [RequireComponent(typeof(UIState))]
    public class DialogBase : MonoBehaviour
    {
        private UIState m_state;
        public UIState UIState { get { return m_state; } }

        [SerializeField] private Text m_caption, m_text;
        [SerializeField] private Button m_buttonA, m_buttonB;

        private System.Action ActionA { get; set; }
        private System.Action ActionB { get; set; }


        private void Awake()
        {
            m_buttonA.onClick.AddListener(() => { ActionA?.Invoke(); });
            m_buttonB.onClick.AddListener(() => { ActionB?.Invoke(); });
            m_state = GetComponent<UIState>();
        }

        public void Show(string caption, string message, string buttonOKCaption, string buttonCancelCaption, System.Action onOK, System.Action onCancel)
        {
            m_caption.text = caption;
            m_text.text = message;

            m_buttonA.GetComponentInChildren<Text>().text = buttonOKCaption;
            m_buttonB.GetComponentInChildren<Text>().text = buttonCancelCaption;

            ActionA = onOK;
            ActionA += () => { UIStack.RelaseDialog(); };
            ActionB = onCancel;
            if(null != ActionB)
            {
                ActionB += () => { UIStack.RelaseDialog(); };
            }

            m_buttonB.gameObject.SetActive(null != ActionB);
        }
    }
}