﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomPropertyDrawer(typeof(PopupAttribute))]
public class PopupDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType == SerializedPropertyType.String)
        {
            PopupAttribute attr = attribute as PopupAttribute;
            int index = 0;
            for(int i = 0; i< attr.m_options.Length; i++)
            {
                if(attr.m_options[i].Equals(property.stringValue))
                {
                    index = i;
                    break;
                }
            }

            index = EditorGUI.Popup(position, index, attr.m_options);
        }
        else
        {
            base.OnGUI(position, property, label);
        }
    }
}
