public static class Scenes 
{
	public static string Launch{ get { return "Launch"; } }
	public static string Init{ get { return "Init"; } }
	public static string Menu{ get { return "Menu"; } }
	public static string Level{ get { return "Level"; } }
	public static string Sandbox{ get { return "Sandbox"; } }

	public static string[] SceneArray = new  string[]
	{
		"Launch",
		"Init",
		"Menu",
		"Level",
		"Sandbox",
	};

}
