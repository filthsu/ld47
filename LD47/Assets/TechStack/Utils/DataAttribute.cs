﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataAttribute : PropertyAttribute
{
    public System.Type databaseType;

    public DataAttribute(System.Type databasetype)
    {
        this.databaseType = databasetype;
    }
}
