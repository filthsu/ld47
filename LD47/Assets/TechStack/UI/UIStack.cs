﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace TechStack.UI
{
    public class UIStack : MonoBehaviour
    {
        #region Dialog system

        private class DialogQueueItem
        {
            public string m_caption;
            public string m_message;
            public string m_okCaption;
            public string m_cancelCaption;
            public System.Action m_onOK;
            public System.Action m_onCancel;
        }

        private DialogBase m_dialog;
        private Queue<DialogQueueItem> m_dialogQueue;
        public static int DialogQueueSize { get { return instance.m_dialogQueue.Count; } }
        public static bool IsDialogVisible { get; private set; }
        public static void ShowDialog(string caption, string message, string okButtonCaption, string cancelButtonCaption, System.Action onOK, System.Action onCancel)
        {
            if (IsDialogVisible)
            {
                instance.m_dialogQueue.Enqueue(new DialogQueueItem()
                {
                    m_caption = caption,
                    m_message = message,
                    m_okCaption = okButtonCaption,
                    m_cancelCaption = cancelButtonCaption,
                    m_onOK = onOK,
                    m_onCancel = onCancel
                });
            }
            else
            {
                ShowDialogInternal(new DialogQueueItem()
                {
                    m_caption = caption,
                    m_message = message,
                    m_okCaption = okButtonCaption,
                    m_cancelCaption = cancelButtonCaption,
                    m_onOK = onOK,
                    m_onCancel = onCancel
                });
            }
        }

        private static void ShowDialogInternal(DialogQueueItem item)
        {
            instance.m_dialog.Show(
                    item.m_caption,
                    item.m_message,
                    item.m_okCaption,
                    item.m_cancelCaption,
                    item.m_onOK,
                    item.m_onCancel);
            instance.Push(instance.m_dialog.UIState);
            instance.OverlayState = instance.m_dialog.UIState;
            IsDialogVisible = true;
        }

        public static void RelaseDialog()
        {
            instance.Pop();
            if(instance.m_dialogQueue.Count > 0)
            {
                ShowDialogInternal(instance.m_dialogQueue.Dequeue());
            }
            else
            {
                IsDialogVisible = false;
                CleanupOverlay();
            }

        }
        #endregion

        #region Scene-based default states
        [System.Serializable]
        public class SceneUIBinding
        {
            [Scene] public string m_scene;
            public UIState m_state;
        }

        [SerializeField] private List<SceneUIBinding> m_sceneBindings = null;
        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            for(int i = 0; i < m_sceneBindings.Count; i++)
            {
                if(scene.name == m_sceneBindings[i].m_scene)
                {
                    Push(m_sceneBindings[i].m_state);
                    return;
                }
            }
        }
        
        #endregion

        public static UIStack instance { get; private set; }
        private Stack<UIState> m_stack;
        private UIState[] m_states;
        private UIState m_currentState;
        /// <summary>
        /// If assigned, this will be used as landing state when launching the game. If null, the state at index of 0 will be used as landing.
        /// </summary>
        [SerializeField] private UIState m_landingState;

        // Start is called before the first frame update
        void Awake()
        {
            instance = this;
            m_stack = new Stack<UIState>();
            m_states = GetComponentsInChildren<UIState>(true);

            if (null == m_landingState)
            {
                if (m_states.Length > 0)
                {
                    m_landingState = m_states[0];
                }
            }
            Push(m_landingState);
            SceneManager.sceneLoaded += this.OnSceneLoaded;
            m_dialog = GetComponentInChildren<DialogBase>();
            m_dialogQueue = new Queue<DialogQueueItem>();
        }

        private void Start()
        {
        }

        private void PushInternal(UIState state)
        {
            if (null == state) return;
            if(state == m_currentState)
            {
                Debug.LogWarning("UI: Trying to push same state twice!");
                return;
            }
            if(null != m_currentState)
            {
                m_stack.Push(m_currentState);
                m_currentState.SetActiveState(false);
            }
            state.SetActiveState(true);
            m_currentState = state;
        }

        private void PopInternal()
        {
            if(m_stack.Count > 0)
            {
                UIState nextState = m_stack.Pop();
                m_currentState?.SetActiveState(false);
                nextState.SetActiveState(true);
                m_currentState = nextState;
            }
        }
    
        public void Pop()
        {
            PopInternal();
        }

        private void LateUpdate()
        {
            if (null != OverlayState)
            {
                OverlayState.transform.SetAsLastSibling();
            }
        }

        /// <summary>
        /// Keeps the chosen ui state on top regardless of the ui state changes. Helps with dialogs and such..
        /// </summary>
        private UIState OverlayState { get; set; }
        public static void CleanupOverlay()
        {
            instance.OverlayState = null;
        }

        public void Push(UIState state)
        {
            if (null == state) return;
            PushInternal(state);
        }

        public void Push(string stateName)
        {
            for(int i = 0; i < m_states.Length; i++)
            {
                if(m_states[i].gameObject.name == stateName)
                {
                    PushInternal(m_states[i]);
                    return;
                }
            }
        }
    
        public void SendUIMessage(string msg)
        {
            TechStack.App.Application.SendUIMessage(msg);
        }
    }
}