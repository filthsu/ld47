﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DeveloperMenuItemAttribute : Attribute
{
    public string buttonCaption = "";

    public DeveloperMenuItemAttribute(){
        buttonCaption = "";
    }

    public DeveloperMenuItemAttribute(string caption)
    {
        buttonCaption = caption;
    }
}
