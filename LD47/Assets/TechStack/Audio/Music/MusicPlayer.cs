﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEditor;

namespace TechStack.Audio.Music
{
    [RequireComponent(typeof(AudioSource))]
    public class MusicPlayer : MonoBehaviour
    {
        private static MusicPlayer instance { get; set; }

        public static void Initialize(MusicDatabase playlistsFile)
        {
            instance = new GameObject("Music Player").AddComponent<MusicPlayer>();
            instance.m_playlistsFile = playlistsFile;
        }

        private MusicDatabase m_playlistsFile = null;
        public static MusicDatabase Playlists { get { return instance.m_playlistsFile; } }

        private AudioSource m_audio;
        public AudioSource AudioSource
        {
            get
            {
                if (null == m_audio) m_audio = GetComponent<AudioSource>();
                return m_audio;
            }
        }

        private Playlist m_currentPlaylist;

        private void Internal_SetPlaylist(Playlist playlist)
        {
            if (m_currentPlaylist == playlist) return;
            if (playlist.m_songs.Count == 0) return;
            m_currentPlaylist = playlist;
            AudioSource.Stop();
            if (null != m_current) { StopCoroutine(m_current); }
            m_current = StartCoroutine(LoopPlaylist());
        }

        private void Internal_Stop()
        {
            if(null != m_current)
            {
                StopCoroutine(m_current);
                AudioSource.Stop();
                m_current = null;
            }
        }

        private Coroutine m_current = null;
        private IEnumerator LoopPlaylist()
        {
            for(; ; )
            {
                int next = Random.Range(0, m_currentPlaylist.m_songs.Count);
                AudioSource.PlayOneShot(m_currentPlaylist.m_songs[next]);
                while (AudioSource.isPlaying) { yield return null; }
                yield return null;
            }
        }

        private void Update()
        {
            if (AudioSource && s_volume >= 0)
            {
                AudioSource.volume = s_volume;
            }
        }

        private static float s_volume = -1;
        public static float Volume
        {
            get
            {
                if(s_volume < 0)
                {
                    s_volume = PlayerPrefs.GetFloat("MUSIC_VOLUME", 1.0f);
                }
                return s_volume;
            }
            set
            {
                s_volume = value;
                PlayerPrefs.SetFloat("MUSIC_VOLUME", value);
            }
        }

        public static void Play(int playlistID)
        {
            Play(instance.m_playlistsFile.GetDataAt(playlistID));
        }
        public static void Play(Playlist playlist)
        {
            instance.Internal_SetPlaylist(playlist);
        }

        public static void Stop()
        {
            instance.Internal_Stop();
        }
    }
}