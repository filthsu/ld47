﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LD47Main : MonoBehaviour
{
    [SerializeField] private LD47GameSession m_gameSessionPrefab = null;

    // Start is called before the first frame update
    void Start()
    {
        TechStack.App.Application.OnUIMessage += OnUIMessage;
    }

    private void OnUIMessage(string obj)
    {
        string[] cmd = obj.Split(' ');
        switch (cmd[0])
        {
            case "startgame":
                TechStack.App.Application.RunGameSessionPrefab(m_gameSessionPrefab);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
