﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace TechStack.UI
{
    public class UIState : MonoBehaviour
    {
        public bool IsCurrent { get; private set; }
        public System.Action<bool> OnStateChanged { get; set; }

        internal void SetActiveState(bool state)
        {
            IsCurrent = state;
            if (state)
            {
                transform.SetAsLastSibling();
            }
            OnStateChanged?.Invoke(state);
        }
    }
}