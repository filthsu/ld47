﻿using UnityEngine;
using System.Collections;
using TechStack.DataUtils;

namespace TechStack.Pooling
{
    [System.Serializable]
    public class PoolableData : DataEntry
    {
        public int m_initialSize = 10;
        public GameObject[] m_prefabs;
    }
}