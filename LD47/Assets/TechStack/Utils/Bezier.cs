﻿using UnityEngine;
using System.Collections;

namespace TechStack.Utils
{
    [System.Serializable]
    public class Curve
    {
        [System.Serializable]
        public class Point
        {
            public Vector3 position;
            public float t;

            public Point() { position = Vector3.zero; t = 0; }
            public Point(Vector3 p, float t) { position = p; this.t = t; }
        }

        public Vector3 start, end;
        public Vector3 controlPointA, controlPointB;

        public Vector3 GetPoint(float t)
        {
            t = Mathf.Clamp01(t);
            return Bezier.GetPoint(
                start,
                controlPointA, 
                controlPointB, 
                end, 
                t);
        }

        public Vector3 GetTangent(float t)
        {
            float t2 = t + .1f;
            if(t2 > 1) { t2 = t; t = t - .01f; }

            Vector3 a = GetPoint(t);
            Vector3 b = GetPoint(t2);
            return b - a;
        }
        /// <summary>
        /// Gets approximated closest point on curve
        /// </summary>
        /// <param name="position">Sample position</param>
        /// <param name="stepping">Curve sample count. Higher sample count yields more precise results with a cost of performance</param>
        /// <returns>Point containing information of the closest point on curve to position</returns>
        public Point Closest(Vector3 position, int stepping = 100)
        {
            Point p = new Point(position, 1f);
            float dMin = 100000000f;
            for(int i = 0; i <= stepping; i++)
            {
                float t = (float)i / (float)stepping;
                Vector3 pos = GetPoint(t);
                float d = Vector3.Distance(pos, position);
                if(d < dMin)
                {
                    dMin = d;
                    p.position = pos;
                    p.t = t;
                }
            }
            return p;
        }

        public void Closest(Point p, Vector3 position, int stepping = 100)
        {
            float dMin = 100000000f;
            for (int i = 0; i <= stepping; i++)
            {
                float t = (float)i / (float)stepping;
                Vector3 pos = GetPoint(t);
                float d = Vector3.Distance(pos, position);
                if (d < dMin)
                {
                    dMin = d;
                    p.position = pos;
                    p.t = t;
                }
            }
        }
    }

    public static class Bezier
    {
        public static float GetLength(Vector3 start, Vector3 tan, Vector3 end, float stepping = .1f)
        {
            float len = 0;
            stepping = Mathf.Clamp01(stepping);
            for (float t = 0; t < 1.0; t += stepping)
            {
                float t0 = t + stepping;
                Vector3 s0 = GetPoint(start, tan, end, t);
                Vector3 s1 = GetPoint(start, tan, end, t0);
                len += Vector3.Distance(s0, s1);
            }
            return len;
        }

        public static float GetLength(Vector3 start, Vector3 tanStart, Vector3 tanEnd, Vector3 end, float stepping = .1f)
        {
            float len = 0;
            stepping = Mathf.Clamp01(stepping);
            for (float t = 0; t < 1.0; t += stepping)
            {
                float t0 = t + stepping;
                Vector3 s0 = GetPoint(start, tanStart, tanEnd, end, t);
                Vector3 s1 = GetPoint(start, tanStart, tanEnd, end, t0);
                len += Vector3.Distance(s0, s1);
            }
            return len;
        }

        public static Vector3 GetPoint(Vector3 start, Vector3 tan, Vector3 end, float t)
        {
            t = Mathf.Clamp01(t);
            float oneMinusT = 1f - t;
            return
                oneMinusT * oneMinusT * start +
                2f * oneMinusT * t * tan +
                t * t * end;
        }

        public static Vector3 GetFirstDerivative(Vector3 start, Vector3 tan, Vector3 end, float t)
        {
            return
                2f * (1f - t) * (tan - start) +
                2f * t * (end - tan);
        }

        public static Vector3 GetPoint(Vector3 start, Vector3 tanStart, Vector3 tanEnd, Vector3 end, float t)
        {
            t = Mathf.Clamp01(t);
            float OneMinusT = 1f - t;
            return
                OneMinusT * OneMinusT * OneMinusT * start +
                3f * OneMinusT * OneMinusT * t * tanStart +
                3f * OneMinusT * t * t * tanEnd +
                t * t * t * end;
        }

        public static Vector3 GetFirstDerivative(Vector3 start, Vector3 tanStart, Vector3 tanEnd, Vector3 end, float t)
        {
            t = Mathf.Clamp01(t);
            float oneMinusT = 1f - t;
            return
                3f * oneMinusT * oneMinusT * (tanStart - start) +
                6f * oneMinusT * t * (tanEnd - tanStart) +
                3f * t * t * (end - tanEnd);
        }



        public static Vector3 Project (Vector3 a, Vector3 b, Vector3 c, Vector3 d, Vector3 point)
        {
            Vector3 closest = Vector3.zero;
            float dClosest = 100000000;
            float tClosest = -1;
            for(int i = 0; i < 10; i++)
            {
                float t = (float)i / 10.0f;
                Vector3 p = GetPoint(a, b, c, d, t);
                float dist = Vector3.Distance(point, p);
                if(dist < dClosest)
                {
                    closest = p;
                    tClosest = t;
                }
            }
            return closest;
            /*
                 // step 1: coarse check
      var LUT = this.getLUT(), l = LUT.length-1,
          closest = utils.closest(LUT, point),
          mdist = closest.mdist,
          mpos = closest.mpos;
      if (mpos===0 || mpos===l) {
        var t = mpos/l, pt = this.compute(t);
        pt.t = t;
        pt.d = mdist;
        return pt;
      }

      // step 2: fine check
      var ft, t, p, d,
          t1 = (mpos-1)/l,
          t2 = (mpos+1)/l,
          step = 0.1/l;
      mdist += 1;
      for(t=t1,ft=t; t<t2+step; t+=step) {
        p = this.compute(t);
        d = utils.dist(point, p);
        if (d<mdist) {
          mdist = d;
          ft = t;
        }
      }
      p = this.compute(ft);
      p.t = ft;
      p.d = mdist;
      return p;
             */

            return point;
        }
    }
}