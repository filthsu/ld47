﻿using System.Collections;
using System.Collections.Generic;
using TechStack.Audio;
using TechStack.Audio.Music;
using TechStack.Pooling;
using TechStack.UI;
using TechStack.VFX;
using UnityEngine;
namespace TechStack.App
{
    public class AppData : ScriptableObject
    {
        [SerializeField] private UIStack m_UI;
        [SerializeField] private List<GameObject> m_persistents;
        public PoolDatabase m_pools;
        public VFXDatabase m_vfx;
        public AudioDatabase m_audio;
        public MusicDatabase m_music;
        internal void InitializePersistents()
        {
            if(null != m_UI)
            {
                UIStack ui = Instantiate(m_UI);
                DontDestroyOnLoad(ui.gameObject);
            }
            for (int i = 0; i < m_persistents.Count; i++)
            {
                if (null == m_persistents[i]) continue;
                GameObject go = Instantiate(m_persistents[i]);
                DontDestroyOnLoad(go);
            }
        }

        internal void InitializeDatabases()
        {
            if (null != m_pools) PoolingManager.Initialize(m_pools);
            if (null != m_vfx) VFXManager.Initialize(m_vfx);
            if (null != m_audio) AudioManager.Initialize(m_audio);
            if (null != m_music) MusicPlayer.Initialize(m_music);
        }
    }
}