﻿using TechStack.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TechStack.VFX
{
    /// <summary>
    /// A standard fire-and-forget VFX efffect handler. Plays specified effects when gameobject is activated and then returns to dormant state ready to be recycled.
    /// </summary>
    public class VFXComponent : MonoBehaviour
    {
        [Tooltip("Time after which the object can be recycled. Usually the length of the effect + some grace time.")]
        [SerializeField] private float m_duration = 5;
        public float Duration { get => m_duration; set => m_duration = value; }
        private float m_timer;

        [SerializeField] private ParticleSystem m_particleSystem = null;

        [SerializeField]
        [Data(typeof(AudioDatabase))]
        private int m_audioEffect = 0;

        /// <summary>
        /// Assign a transform to have VFX effect follow a transform
        /// </summary>
        public Transform BindTransform { get; set; }
        public Vector3 BindOffset { get; set; }

        private void Awake()
        {
            if (!m_particleSystem)
            {
                m_particleSystem = GetComponentInChildren<ParticleSystem>();
            }
        }

        private void OnEnable()
        {
            m_timer = 0;
            if(null != m_particleSystem)
            {
                m_particleSystem.Play();
            }
            AudioManager.Play(m_audioEffect, transform.position);
        }

        private void Update()
        {
            m_timer += Time.deltaTime;
            if(null != BindTransform)
            {
                transform.position = BindTransform.TransformPoint(BindOffset);
                transform.rotation = BindTransform.rotation;
            }
            if(m_timer >= m_duration)
            {
                BindTransform = null;
                gameObject.SetActive(false);
            }
        }
    }
}