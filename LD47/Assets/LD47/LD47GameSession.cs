﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class LD47GameSession : TechStack.Game.GameSession
{
    public static LD47GameSession Instance { get; private set; }

    private LD47Character m_playerCharacter;

    [Header("Map")]
    [SerializeField]
    [Scene]
    private string m_gameScene = "";

    [Header("Paranoia")]
    [SerializeField] private float m_paranoiaGainPerCharacter = .01f;
    [SerializeField] private float m_paranoiaGainOverTime = .02f;
    [SerializeField] private float m_paranoiaReleaseOverTime = .01f;
    private float m_paranoia;
    public float Paranoia
    {
        get { return Mathf.Clamp01(m_paranoia); }
        set { m_paranoia = value; }
    }

    [Header("Camera Setup")]
    [SerializeField] private Camera m_cameraPrefab = null;
    [SerializeField] private float m_cameraDistance = 20;
    private Vector3 m_cameraAngle = new Vector3(45, 0, 0);
    private Camera m_camera;

    private bool m_characterStateChangeRequested = false;

    protected internal override IEnumerator Initialize()
    {
        TechStack.UI.UIStack.instance.Push("Loading");
        Instance = this;
        yield return UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(m_gameScene);
        yield return new WaitForEndOfFrame();

        BusRoute[] routes = FindObjectsOfType<BusRoute>();
        for(int i =0;i < routes.Length; i++)
        {
            Bus bus = PoolingManager.Pop(Pools.Bus, routes[i].m_nodes[0].transform.position).GetComponent<Bus>();
            bus.route = routes[i];
        }

        BusStop[] stops = FindObjectsOfType<BusStop>();
        for(int i = 0; i < stops.Length; i++)
        {
            if(stops[i].IsStartPoint)
            {
                m_playerCharacter = PoolingManager.Pop(Pools.Character, stops[i].transform.position).GetComponent<LD47Character>();
                stops[i].Add(m_playerCharacter);
                break;
            }
        }

        m_camera = Instantiate(m_cameraPrefab, transform);
    }

    protected internal override IEnumerator Run()
    {
        TechStack.UI.UIStack.instance.Push("HUD");
        for (; ; )
        {
            if (null != m_playerCharacter)
            {
                if (null != m_playerCharacter.Bus)
                {
                    if (m_characterStateChangeRequested)
                    {
                        BusStop stop = m_playerCharacter.Bus.ScanBusStop();
                        if (null != stop)
                        {
                            m_playerCharacter.Bus.Remove(m_playerCharacter);
                            stop.Add(m_playerCharacter);
                            m_characterStateChangeRequested = false;
                        }
                    }

                    if (null != m_playerCharacter.Bus)
                    {
                        float targetParanoia = ((m_playerCharacter.Bus.Characters.Count - 1) > 0 ? 1 : 0);
                        if (targetParanoia > m_paranoia)
                        {
                            Paranoia = Mathf.MoveTowards(Paranoia, targetParanoia, Time.deltaTime * m_paranoiaGainOverTime * (m_playerCharacter.Bus.Characters.Count - 1) * m_paranoiaGainPerCharacter);
                        }
                        else
                        {
                            Paranoia = Mathf.MoveTowards(Paranoia, targetParanoia, Time.deltaTime * m_paranoiaReleaseOverTime);
                        }
                    }
                }
                else if (null != m_playerCharacter.BusStop)
                {
                    if (m_characterStateChangeRequested)
                    {
                        Bus bus = m_playerCharacter.BusStop.ScanBus();
                        if(null != bus)
                        {
                            m_playerCharacter.BusStop.Remove(m_playerCharacter);
                            bus.Add(m_playerCharacter);
                            m_characterStateChangeRequested = false;
                        }
                    }
                    if (null != m_playerCharacter.BusStop)
                    {
                        float targetParanoia = ((m_playerCharacter.BusStop.Characters.Count - 1) > 0 ? 1 : 0);
                        if (targetParanoia > m_paranoia)
                        {
                            Paranoia = Mathf.MoveTowards(Paranoia, targetParanoia, Time.deltaTime * m_paranoiaGainOverTime * (m_playerCharacter.BusStop.Characters.Count - 1) * m_paranoiaGainPerCharacter);
                        }
                        else
                        {
                            Paranoia = Mathf.MoveTowards(Paranoia, targetParanoia, Time.deltaTime * m_paranoiaReleaseOverTime);
                        }
                    }
                }
                yield return null;
            }
        }
    }

    protected internal override IEnumerator Cleanup()
    {
        yield return null;
    }

    private void LateUpdate()
    {
        m_cameraAngle.y = Mathf.Repeat(m_cameraAngle.y + Mouse.current.delta.x.ReadValue() * 100 * Time.deltaTime, 360f);
        m_cameraAngle.x = Mathf.Clamp(m_cameraAngle.x + Mouse.current.delta.y.ReadValue() * 100 * Time.deltaTime, 25, 89.9f);
       if(m_playerCharacter && m_camera)
        {
            m_camera.transform.position = Vector3.Lerp(m_camera.transform.position,
                m_playerCharacter.transform.position + (Quaternion.Euler(m_cameraAngle)*Vector3.back * m_cameraDistance),
                Time.deltaTime * 25);

            m_camera.transform.rotation = Quaternion.Slerp(m_camera.transform.rotation,
                Quaternion.Euler(m_cameraAngle),
                Time.deltaTime * 25);

            if(null != m_playerCharacter.Bus)
            {
                m_playerCharacter.transform.position = m_playerCharacter.Bus.transform.position;
            }
        }

        if (Mouse.current.leftButton.wasPressedThisFrame)
        {
            m_characterStateChangeRequested = !m_characterStateChangeRequested;
        }
    }
}
