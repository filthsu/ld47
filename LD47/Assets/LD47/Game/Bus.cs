﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bus : MonoBehaviour
{
    public BusRoute route { get; set; }
    private int m_routeWaypointIndex = 0;
    [SerializeField] private float m_moveSpeed = 10;
    [SerializeField] private float m_turnSpeed = 10;
    [SerializeField] private float m_steeringDistance = 3;
    public List<LD47Character> Characters { get; set; }

    public void Add(LD47Character character)
    {
        Characters.Add(character);
        character.Bus = this;
    }

    public void Remove(LD47Character character)
    {
        Characters.Remove(character);
        character.Bus = null;
    }

    // Start is called before the first frame update
    void Start()
    {
        Characters = new List<LD47Character>();
    }

    public BusStop ScanBusStop()
    {
        int layer = (1 << 10);
        Collider[] c = Physics.OverlapSphere(transform.position, 10, layer);
        BusStop result = null;
        float maxDot = 0;
        float minDistance = 11;
        if (c.Length > 0)
        {
            for (int i = 0; i < c.Length; i++)
            {
                BusStop sample = c[i].GetComponentInParent<BusStop>();
                Vector3 delta = sample.transform.position - transform.position;
                float dot = Vector3.Dot(transform.forward, delta.normalized);
                if (dot > maxDot)
                {
                    if (delta.magnitude < minDistance)
                    {
                        maxDot = dot;
                        minDistance = delta.magnitude;
                        result = sample;
                    }
                }
            }
        }
        return result;
    }

    public void ApproachStop() 
    {
    }

    // Update is called once per frame
    void Update()
    {
        Transform waypoint = route.m_nodes[m_routeWaypointIndex].transform;
        Vector3 delta = waypoint.position - transform.position;

        float distance = Mathf.Min(delta.magnitude, m_moveSpeed * Time.deltaTime);
        transform.position += delta.normalized * distance;
        if (delta.magnitude <= Time.deltaTime * m_moveSpeed)
        {
            m_routeWaypointIndex = (m_routeWaypointIndex + 1) % route.m_nodes.Count;
            //waypoint = route.m_nodes[m_routeWaypointIndex].transform;
           // delta = waypoint.position - transform.position;
        }
        Quaternion nextAngle = Quaternion.LookRotation(delta, Vector3.up);
        if (delta.magnitude < m_steeringDistance)
        {
             waypoint = route.m_nodes[(m_routeWaypointIndex + 1) % route.m_nodes.Count].transform;
            delta = waypoint.position - transform.position;
            nextAngle = Quaternion.LookRotation(delta, Vector3.up);
        }
        transform.rotation = Quaternion.Slerp(transform.rotation, nextAngle, m_turnSpeed * Time.deltaTime);


       
    }
}
