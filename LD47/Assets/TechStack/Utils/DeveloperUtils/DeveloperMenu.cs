﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;
namespace TechStack.Utils.DeveloperUtils
{
    /// <summary>
    /// Developer menu. Initialized automatically on editor & development builds.
    /// </summary>
    public class DeveloperMenu : MonoBehaviour
    {
        List<MethodInfo> m_methods;
        public static bool IsVisible { get; set; }

        [RuntimeInitializeOnLoadMethod]
        static void Initialize()
        {
            if (Application.isEditor || Debug.isDebugBuild)
            {
                DeveloperMenu menu = new GameObject("DeveloperMenu").AddComponent<DeveloperMenu>();
                DontDestroyOnLoad(menu.gameObject);
            }
        }

        void OnJKCommand(string cmd)
        {
            for(int i =0; i< m_methods.Count; i++)
            {
                if(cmd.ToLower() == m_methods[i].DeclaringType.Name.ToLower() + "::" + m_methods[i].Name.ToLower())
                {
                    m_methods[i].Invoke(this, null);
                    return;
                }
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            m_methods = new List<MethodInfo>();
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach(Assembly assembly  in assemblies)
            {
                Type[] types = assembly.GetTypes();
                foreach(Type type in types)
                {
                    MethodInfo[] methods = type.GetMethods();
                    foreach(MethodInfo method in methods)
                    {
                        if(method.IsStatic && null != method.GetCustomAttribute<DeveloperMenuItemAttribute>())
                        {
                            m_methods.Add(method);
                        }
                    }
                }
            }
            DontDestroyOnLoad(gameObject);
            IsVisible = false;
        }

        private void Update()
        {
            if(UnityEngine.InputSystem.Keyboard.current.f1Key.wasPressedThisFrame)
            {
                IsVisible = !IsVisible;
            }
        }

        Vector2 m_scrollPosition;
        private void OnGUI()
        {
            if (IsVisible)
            {
                GUILayout.BeginArea(new Rect(0, 0, 300, Screen.height));
                m_scrollPosition = GUILayout.BeginScrollView(m_scrollPosition);
                GUILayout.BeginVertical();
                foreach (MethodInfo method in m_methods)
                {
                    if (GUILayout.Button(method.DeclaringType + "::" + method.Name))
                    {
                        method.Invoke(null, null);
                    }
                }
                GUILayout.EndVertical();
                GUILayout.EndScrollView();
                GUILayout.EndArea();
            }
        }
    }
}