﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Globalization;

namespace TechStack.Utils
{
    public class Logger : MonoBehaviour
    {
        private static StreamWriter writer;

        [RuntimeInitializeOnLoadMethod]
        public static void Initialize()
        {
#if !UNITY_EDITOR 
            GameObject go = new GameObject("Logger");
            DontDestroyOnLoad(go);
            go.AddComponent<Logger>();
#endif
        }

        private string m_logPath;

        private void Start()
        {
            string pathRoot = Application.dataPath + "/../Logs/";
            
            if (!Directory.Exists(pathRoot))
            {
                Directory.CreateDirectory(pathRoot);
            }
            
            m_logPath = pathRoot + Application.productName + "_" + DateTime.Now.ToString("ddMMMyyyyHHmmss", CultureInfo.CreateSpecificCulture("en-us")) + ".log";
            writer = new StreamWriter(File.Create(m_logPath));
            Application.logMessageReceived += OnLogMessage;
        }

        private void OnLogMessage(string condition, string stackTrace, LogType type)
        {
            writer.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " | " + type + ": " + condition);
            if (stackTrace.Length > 0)
            {
                string[] trace = stackTrace.Split('\n');
                for (int i = 0; i < trace.Length; i++)
                {
                    writer.WriteLine("\t" + trace[i]);
                }
            }
        }

        private void OnApplicationQuit()
        {
            if(null != writer)
                writer.Close();
        }
    }
}