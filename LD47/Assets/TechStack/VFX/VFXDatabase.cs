﻿using TechStack.DataUtils;
using TechStack.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace TechStack.VFX
{
    [CreateAssetMenu(fileName = "VFX.asset", menuName = "TechStack/Data/VFX Database")]
    public class VFXDatabase : TechStack.DB.DataBase<PoolableData> 
    {

        [EditorButton("Add Selected Prefabs")]
        private void AddSelectedPrefabs()
        {
#if UNITY_EDITOR
            foreach(GameObject go in UnityEditor.Selection.gameObjects)
            {
                PoolableData obj = AddEntry();
                obj.name = go.name;
                obj.m_prefabs = new GameObject[] { go };
            }
#endif
        }

        protected override void OnValidate()
        {
            base.OnValidate();
#if UNITY_EDITOR
            foreach (PoolableData d in Data)
            {
                if (null == d) continue;
                foreach (GameObject go in d.m_prefabs)
                {
                    if (null == go) continue;
                    if (!go.GetComponent<VFXComponent>())
                    {
                        go.AddComponent<VFXComponent>();
                        EditorUtility.SetDirty(go);
                    }
                }
            }
#endif
        }
    }
}