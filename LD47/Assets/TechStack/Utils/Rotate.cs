﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace TechStack.Utils
{
    public class Rotate : MonoBehaviour
    {
        [SerializeField] private Vector3 m_rotationSpeed = Vector3.zero;
        [SerializeField] private bool m_scaledTime = true;


        // Update is called once per frame
        void Update()
        {
            if (m_scaledTime)
            {
                transform.Rotate(m_rotationSpeed * Time.deltaTime);
            }
            else
            {
                transform.Rotate(m_rotationSpeed * Time.unscaledDeltaTime);
            }
        }
    }
}