﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TechStack.DataUtils;
using UnityEditor.Animations;
using TechStack.DB;
using NUnit.Framework.Constraints;

[CustomPropertyDrawer(typeof(DataAttribute))]
public class DataPickerDrawer : PropertyDrawer
{
    public DataInfo[] info;
    public string[] names;
    public int index;
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        DataAttribute prop = attribute as DataAttribute;

        EditorGUI.BeginProperty(position, label, property);
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        if (null == names)
        {
            info = DataFileUtility.GetDBNames(prop.databaseType);
            names = new string[info.Length];
            for (int i = 0; i < info.Length; i++)
            {
                names[i] = info[i].name;
            }
        }

        index = DataFileUtility.FindIndex(property.intValue, info);
        index = EditorGUI.Popup(position, index, names);
        property.intValue = DataFileUtility.FindID(index, info);

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }

    public class DataWrap
    {
        public System.Type dbType;
        public DataInfo[] data;
        public string[] names;
    }

    private static Dictionary<System.Type, DataWrap> s_editorTempData = new Dictionary<System.Type, DataWrap>();

    public static void RefreshDataPickers()
    {
        s_editorTempData = new Dictionary<System.Type, DataWrap>();
    }

    public static string GetDataEntryName(System.Type databaseType, int ID)
    {
        if (!s_editorTempData.ContainsKey(databaseType))
        {
            DataWrap wrap = new DataWrap()
            {
                dbType = databaseType
            };

            wrap.data = DataFileUtility.GetDBNames(databaseType);
            wrap.names = new string[wrap.data.Length];
            for (int i = 0; i < wrap.data.Length; i++)
            {
                wrap.names[i] = wrap.data[i].name;
            }
            s_editorTempData.Add(databaseType, wrap);
        }


        DataWrap temp = s_editorTempData[databaseType];

        try
        {
            int index = DataFileUtility.FindIndex(ID, temp.data);
            return temp.names[index];
        }
        catch { }

        return "";
    }

    public static int DrawDataPicker(System.Type databaseType, int value, string label)
    {
        if (!s_editorTempData.ContainsKey(databaseType))
        {
            DataWrap wrap = new DataWrap()
            {
                dbType = databaseType
            };

            wrap.data = DataFileUtility.GetDBNames(databaseType);
            wrap.names = new string[wrap.data.Length];
            for (int i = 0; i < wrap.data.Length; i++)
            {
                wrap.names[i] = wrap.data[i].name;
            }
            s_editorTempData.Add(databaseType, wrap);
        }

        DataWrap temp = s_editorTempData[databaseType];

        int index = DataFileUtility.FindIndex(value, temp.data);
        index = EditorGUILayout.Popup(label, index, temp.names);

        return index;

        /*
        DataBase<DataEntry> temp = DataFileUtility.GetDatabase(databaseType);

        int index = DataFileUtility.FindIndex(value, temp.m_editorInfo);
        index = EditorGUILayout.Popup(label, index, temp.m_editorNames);

        return index;
        */
    }
}
