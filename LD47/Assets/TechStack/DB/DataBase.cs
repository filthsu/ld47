﻿using UnityEngine;
using System.Collections.Generic;
using TechStack.DataUtils;
using System;

namespace TechStack.DB
{
    public class DataBase<T> : ScriptableObject where T : DataEntry
    {

        [System.NonSerialized]
        public DataInfo[] m_editorInfo;
        [System.NonSerialized]
        public string[] m_editorNames;

        public void Editor_UpdateLookupLists()
        {
            m_editorNames = new string[Data.Count + 1];
            m_editorNames[0] = "None";
            for(int i = 0; i < Data.Count; i++)
            {
                m_editorNames[i + 1] = Data[i].name;
            }

            m_editorInfo = new DataInfo[Data.Count + 1];
            m_editorInfo[0] = new DataInfo() { name = "None", ID = 0 };
        }

        public List<T> data;
        public List<T> Data { get { return data; } }

        public T GetDataAt(int ID)
        {
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].ID == ID) return data[i];
            }
            return null;
        }

        protected T AddEntry()
        {
            T result = Activator.CreateInstance<T>();
            data.Add(result);
            return result;
        }

        public T[] GenerateLookupTable()
        {
            return DataFileUtility.GenerateLookupTable<T>(this);
        }

        protected virtual void OnValidate()
        {
#if UNITY_EDITOR
            DataFileUtility.AssignIDs<T>(data);
#endif
        }

#if UNITY_EDITOR
        [EditorButton("Export Consts File")]
        public void ExportConsts()
        {
            TechStack.DataUtils.DataFileUtility.WriteConsts<T>(data);
        }
#endif
    }
}