﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace JamKit.Utils
{
    /// <summary>
    /// A helper class to store scenes in a consts file
    /// </summary>
    public class SceneConsts
    {
        [MenuItem("TechStack/Utils/Create Scene Consts")]
        public static void CreateSceneConstsFile()
        {
            string path = EditorUtility.SaveFilePanel("Select path", Application.dataPath, "Scenes", "cs");
            if (string.IsNullOrEmpty(path)) return;
            string[]assetGuids =  AssetDatabase.FindAssets("t:scene");
            StreamWriter writer = new StreamWriter(File.Create(path));

            writer.WriteLine("public static class Scenes{");
            for(int i = 0; i < EditorBuildSettings.scenes.Length; i++)
            {
                string key = Path.GetFileNameWithoutExtension(EditorBuildSettings.scenes[i].path);
                writer.WriteLine("\tpublic static string " + key + @"{ get { return """ + key + @"""; } }");
            }
            writer.WriteLine("");
            writer.WriteLine("\tpublic static readonly string[] SceneArray = new string[]");
            writer.WriteLine("\t{");
            for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
            {
                string key = Path.GetFileNameWithoutExtension(EditorBuildSettings.scenes[i].path);
                writer.WriteLine("\t\t" + @"""" + key + @""",");
            }
            writer.WriteLine("\t};");
            writer.WriteLine("}");
            writer.Close();
            AssetDatabase.Refresh();
        }
    }
}