﻿using System.Collections;
using System.Collections.Generic;
using TechStack.Game;
using TechStack.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TechStack.App
{
    public class Application : MonoBehaviour
    {
        #region App state
        public enum State
        {
            Launch,
            Init,
            Ready,
            SessionInit,
            SessionRun,
            SessionFinish,
            SessionCleanedUp,
            ShutdownRequest,
            Shutdown,
            Kill
        }
        #endregion

        #region Static interface
        public static Application Instance { get; private set; }
        /// <summary>
        /// Shuts down the application
        /// </summary>
        public static void Shutdown()
        {
            Instance.StartCoroutine(Instance.ShutdownAsync());
        }
        #endregion

        #region Public events
        public static System.Action<State> OnApplicationStateChange { get; set; }
        #endregion

        #region App data
        private AppData m_appData;
        #endregion

        #region Initialization
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        private void Start()
        {
            StartCoroutine(Initialize());
        }

        IEnumerator Initialize()
        {
            yield return new WaitForEndOfFrame();
            OnApplicationStateChange?.Invoke(State.Launch);

            //TODO: Do all the initial stuff here
            m_appData = Resources.Load<AppData>("AppData");
            if (null != m_appData)
            {
                m_appData.InitializePersistents();
            }
            yield return new WaitForEndOfFrame();
            m_appData.InitializeDatabases();
            yield return new WaitForEndOfFrame();
            yield return SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
            OnApplicationStateChange?.Invoke(State.Init);
            yield return new WaitForEndOfFrame();
            yield return SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
            OnApplicationStateChange?.Invoke(State.Ready);
            yield return new WaitForEndOfFrame();
        }
        #endregion

        #region Shutdown
        public static bool IsShuttingDown { get; private set; }
        private IEnumerator ShutdownAsync()
        {
            if (IsShuttingDown) yield break;
            IsShuttingDown = true;

            OnApplicationStateChange?.Invoke(State.ShutdownRequest);
            yield return new WaitForEndOfFrame();
            OnApplicationStateChange?.Invoke(State.Shutdown);
            yield return null;
            OnApplicationStateChange?.Invoke(State.Kill);
            UnityEngine.Application.Quit();
        }
        #endregion

        #region Game sessions
        public static void RunGameSession<T>(T sessionType) where T : TechStack.Game.GameSession
        {
            return;
            T obj = new GameObject(typeof(T).Name).AddComponent<T>();
            RunGameSession(obj);
        }

        public static void RunGameSessionPrefab(GameSession sessionObject)
        {
            sessionObject = Instantiate(sessionObject);
            DontDestroyOnLoad(sessionObject.gameObject);
            if(null != currentSessionCoroutine)
            {
                Instance.StopCoroutine(currentSessionCoroutine);
                if (null != currentGameSession) Destroy(currentGameSession);
            }
            Instance.StartCoroutine(Instance.RunGameSessionAsync(sessionObject));
        }

        private static Coroutine currentSessionCoroutine;
        private static GameSession currentGameSession;

        private IEnumerator RunGameSessionAsync(GameSession sessionObject)
        {
            currentGameSession = sessionObject;
            OnApplicationStateChange?.Invoke(State.SessionInit);
            yield return sessionObject.Initialize();
            yield return new WaitForEndOfFrame();
            OnApplicationStateChange?.Invoke(State.SessionRun);
            yield return sessionObject.Run();
            yield return new WaitForEndOfFrame();
            OnApplicationStateChange?.Invoke(State.SessionFinish);
            yield return new WaitForEndOfFrame();
            yield return sessionObject.Cleanup();
            OnApplicationStateChange?.Invoke(State.SessionCleanedUp);
        }
        #endregion

        #region UI Messaging system
        public static System.Action<string> OnUIMessage { get; set; }
        public static void SendUIMessage(string msg)
        {
            OnUIMessage?.Invoke(msg);

            string[] cmd = msg.Split(' ');
            switch (cmd[0])
            {
                case "exitapp":
                    UnityEngine.Application.Quit();
                    break;
            }
        }
        #endregion
    }
}