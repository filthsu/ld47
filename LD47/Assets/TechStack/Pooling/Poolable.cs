﻿using UnityEngine;
using System.Collections;
namespace TechStack.Pooling
{
    public class Poolable : MonoBehaviour
    {
        public Pool owner { get; internal set; }
        private Transform m_bindTransform;
        private Vector3 m_bindTransformOffset;
        private Quaternion m_bindTransformAngle;
        private void OnDisable()
        {
            m_bindTransform = null;
            owner.Push(this);
        }

        private void LateUpdate()
        {
            if(null != m_bindTransform)
            {
                transform.position = m_bindTransform.TransformPoint(m_bindTransformOffset);
                transform.rotation = m_bindTransformAngle * m_bindTransform.rotation;
            }
        }

        public void Bind(Transform target)
        {
            m_bindTransform = target;
            m_bindTransformOffset = Vector3.zero;
            m_bindTransformAngle = Quaternion.identity;
        }

        public void Bind(Transform target, Vector3 offset, Quaternion angle, Space space = Space.World)
        {
            m_bindTransform = target;
            switch(space)
            {
                case Space.Self:
                    m_bindTransformOffset = offset;
                    break;
                case Space.World:
                    m_bindTransformOffset = target.InverseTransformPoint(offset);
                    break;
            }
            m_bindTransformAngle = angle;

        }
    }
}