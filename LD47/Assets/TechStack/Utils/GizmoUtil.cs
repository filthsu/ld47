﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
namespace TechStack.Utils
{
    public class GizmoUtil 
    {
       
        public static void DrawCircle(Vector3 center, float radius, int stepping = 36)
        {
            Vector3 a, b;
            float step = 360f / stepping;
            for(int i = 0; i < stepping; i++)
            {
                float angle = step * i * Mathf.Deg2Rad;
                a = center + new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * radius;

                angle = step * ((i + 1) % stepping) * Mathf.Deg2Rad;
                b = center + new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * radius;

                Gizmos.DrawLine(a, b);
            }
        }
    }
}